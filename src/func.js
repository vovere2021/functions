let {listOfPosts, listOfPosts2} = require('./posts.js')

const getSum = (str1, str2) => {
  return typeof str1 !== 'string' && typeof str2 !== 'string' || isNaN(str1) || isNaN(str2) ? false : (+str1 + +str2).toString()
}

const getQuantityPostsByAuthor = (list, authorName) => {
  let postActivity = 0
  let commentActivity = 0
  let listOfComments = []

  postActivity = [...list].filter(post => post.author === authorName)

  let postWithComments = [...list].filter(post => post.comments)
  listOfComments = [...postWithComments].map(post => [...post.comments].filter(comment => comment.author === authorName))

  commentActivity = listOfComments.map(activity => activity.length)

  return `Post:${postActivity.length},comments:${commentActivity.reduce((acc, i) => acc + i)}`
}

const tickets=(people)=> {
  let answer = 'YES'
  let cash = []
  const smallTicketCost = 25
  const middleTicketCost = 50
  const bigTicketCost = 100

  const billSmall = new RegExp(smallTicketCost)
  const billMiddle = new RegExp(middleTicketCost)

  people = people.map(hum => Number(hum))

  for (let visitor of people) {
    if (visitor === smallTicketCost) {
      cash.push(visitor)
    } else if (visitor === middleTicketCost && billSmall.test(cash)) {
      cash.splice(cash.findIndex(item => item === smallTicketCost), 1, visitor)
    } else if (visitor === bigTicketCost) {
      if (billSmall.test(cash) && billMiddle.test(cash)) {      
      cash.splice(cash.findIndex(item => item === middleTicketCost), 1, bigTicketCost)
      cash.splice(cash.findIndex(item => item === smallTicketCost), 1)
      } else if (cash.filter(item => item === smallTicketCost).length >= 3) {
        cash.splice(cash.findIndex(item => item === smallTicketCost), 1)
        cash.splice(cash.findIndex(item => item === smallTicketCost), 1)
        cash.splice(cash.findIndex(item => item === smallTicketCost), 1, bigTicketCost)
      } else {
        answer = 'NO'
      }
    } else {
      answer = 'NO'
    }
  }
  return answer
}

module.exports = {getSum, getQuantityPostsByAuthor, tickets};